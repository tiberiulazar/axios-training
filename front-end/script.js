{
    const state = {
        products: []
    }

    const productsContainer = document.querySelector('.products-container');

    const clearInputsWithClass = (inputClass) => {
        const inputs = document.querySelectorAll(inputClass);
        inputs.forEach(input => input.value = "")
    }

    window.addEventListener("click", async (e) => {
        // add product
        if (e.target.matches(".form__button--product")) {
            const body = {};
            const inputs = document.querySelectorAll(".form__input--product");
            inputs.forEach(input => {
                body[input.getAttribute("name")] = input.value;
            });
            body.price = parseFloat(body.price);
            body.quantity = parseInt(body.quantity);
            body.customId = cuid();

            if (!body.price || !body.quantity || !body.price) {
                toastr.error("There are empty fields!");
                return;
            }

            axios.post("/addProduct", body)
                .then(res => {
                    displayProduct(body);
                    clearInputsWithClass(".form__input--product");
                    closeAddPopup();
                    toastr.success("Product added!");
                })
                .catch(err => console.log(err))

            // delete product
        } else if (e.target.matches(".product__delete")) {
            const product = e.target.closest(".product");
            const productId = product.dataset.id;
            //delete from backend
            axios.delete(`/removeProduct/${productId}`).then(res => {
                //delete from state
                state.products = state.products.filter(product => product.id !== productId);
                //delete from app frontend
                product.parentElement.removeChild(product);
            }).catch(err => console.log(err));

            // show popup
        } else if (e.target.matches(".add__toggler")) {
            displayAddPopup();
        } else if (e.target.matches(".close-popup")) {
            closeAddPopup();
        }
    })

    // get all products
    const getProducts = async () => {
        await axios.get('/getAllProducts').then(res => state.products = res.data).catch(err => console.log(err))
    }

    // search functionality
    window.addEventListener("keyup", e => {
        if (e.target.matches('.products__search')) {
            const inputValue = e.target.value;
            if (inputValue.length >= 3) {
                const filteredProducts = state.products.filter(product => product.name.includes(inputValue));
                productsContainer.innerHTML = '';
                renderProducts(filteredProducts);
            } else if (inputValue.length === 0) {
                productsContainer.innerHTML = '';
                renderProducts(state.products);
            }
        }
    })

    // display one product
    const displayProduct = ({ customId, name, price, quantity }) => {
        const markup = `
        <div class="product" data-id="${customId}">
            <div class="product__cell">${customId}</div>
            <div class="product__cell">${name}</div>
            <div class="product__cell">${price}</div>
            <div class="product__cell">${quantity}</div>
            <div class="product__cell product__cell--delete">
                <button class="product__delete">Delete</button>
            </div>
        </div>`;
        productsContainer.insertAdjacentHTML("afterbegin", markup)
    }

    // display add popup
    const displayAddPopup = () => {
        const markup = `
        <div class="add-popup">
            <div class="form">
                <div class="form__group">
                    <label for="form-product" class="form__label">Product name</label>
                    <input type="text" id="form-product" name="name" class="form__input form__input--product">
                </div>
                <div class="form__group">
                    <label for="form-price" class="form__label">Price</label>
                    <input type="text" name="price" id="form-price" class="form__input form__input--product">
                </div>
                <div class="form__group">
                    <label for="form-quantity" class="form__label">Quantity</label>
                    <input type="text" name="quantity" id="form-quantity" class="form__input form__input--product">
                </div>
                <button class="form__button form__button--product">Add product</button>
                <div class="close-popup">&#x2715</div>
            </div>
            
        </div>
        `;

        document.querySelector('body').insertAdjacentHTML("afterbegin", markup);
    }

    //close add popup
    const closeAddPopup = () => {
        const popup = document.querySelector(".add-popup");
        popup.parentElement.removeChild(popup);
    }

    // display every product
    const renderProducts = (products) => {
        products.forEach(product => displayProduct(product));
    }

    // render everythinh when open app
    window.addEventListener("load", async () => {
        await getProducts();
        renderProducts(state.products);
    })
}